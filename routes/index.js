var ip = require('ip');
var express = require('express');
var router = express.Router();
var myip = ip.address();
var os = require('os');
var oshostname = os.hostname();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { ip: 'Someone is yelling on ' + myip + ' from pod: ' + oshostname});
});

module.exports = router;

console.log('Test me at http://localhost:3000');

//console.log('...oh and... Someone is yelling on ' + myip);
console.log('...oh and... Someone is yelling on ' + myip + ' from pod: ' + oshostname);