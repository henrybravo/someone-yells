# someone-yells

This is a little demo single page webapp build with nodejs + express + pug. It loads an image hosted in s3 and displays the server's IP address and hostname. This demo app is used across multiple repositories to show AWS container services, such as (microservices-ecs-cloudformatio)[https://gitlab.com/henrybravo/microservices-ecs-cloudformation], [someone-yells-k8s](https://gitlab.com/henrybravo/someone-yells-k8s), [helm chart](https://gitlab.com/henrybravo/someoneyells-helm), [someone-yells-cdk](https://gitlab.com/henrybravo/someone-yells-cdk)

## suggested dev environment that runs in a container

To use the offical node container to develop and build the app, pull the image:

```bash
$ docker pull node
```

Start the node container (build on debian jessie) - where we expose port 3000 to localhost so we can preview the app in the browser too

```bash
$ docker run -it -p 3000:3000 node bash
```

In the container

```bash
root@fb12900d0e7d:/# apt-get update -y && apt-get install -y vim
```

Never work as root

```bash
root@fb12900d0e7d:/# su - node
```

Clone this repo

```bash
node@fb12900d0e7d:~$ git clone https://gitlab.com/henrybravo/someone-yells.git
Cloning into 'someone-yells'...
```

```bash
node@fb12900d0e7d:~$ cd someone-yells
```

## build the app

as this repo has an allready installed app, we only pull updates

```bash
node@fb12900d0e7d:~/someone-yells$ npm update
```

check if there are findings in the module tree

```bash
node@fb12900d0e7d:~/someone-yells$ npm audit
```

if any, see if you can safely fix the findings in the module tree

```bash
node@fb12900d0e7d:~/someone-yells$ npm audit fix
```

## run and test app

run the build (in the node container)

```bash
node@fb12900d0e7d:~/someone-yells$ DEBUG=* npm start &
```

Test in a browser

```bash
node@fb12900d0e7d:~/someone-yells$ curl localhost:3000
<!DOCTYPE html><html><head><title></title><link rel="stylesheet" href="https://refarch-demos.s3.amazonaws.com/stylesheets/style.css"></head><body><div id="main"><img src="https://refarch-demos.s3.amazonaws.com/images/nytimes.png"><img src="https://refarch-demos.s3.amazonaws.com/images/cloud.jpg" width="75%"><h2>Someone is yelling on 172.17.0.2 from host: fb12900d0e7d</h2></div></body></html>node@fb12900d0e7d:~/someone-yells$ GET / 200 219.137 ms - 394
```

as we exposed the app port to localhost you can access the app from the local host's webbrowser too -> http://localhost:3000

## deploy to Amazon EKS or Amazon ECS

* [ECS reference architecture example](https://gitlab.com/henrybravo/microservices-ecs-cloudformation)

* [EKS deployment example](https://gitlab.com/henrybravo/someone-yells-k8s)

